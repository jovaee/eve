# Build frontend
FROM node:15.3-alpine

WORKDIR /app

COPY app/package.json ./
COPY app/package-lock.json ./

RUN npm install
RUN npm install react-scripts -g

COPY app/ ./

RUN npm run build

# API
FROM python:3.7

WORKDIR /eve

COPY --from=0 /app/build app/build/

EXPOSE 8000

# TODO: This copy can be made to only copy server files
COPY . .

RUN pip install -U pip && pip install -r requirements.txt

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "8000"]