import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse, StreamingResponse 
from fastapi.requests import Request
from fastapi.staticfiles import StaticFiles

from models.models import Track

from typing import List

from sqlalchemy import text
from sqlalchemy.ext.asyncio import create_async_engine

BYTES_PER_RESPONSE = 100000  # 100KB
DEBUG = os.environ.get("DEBUG", False)
DB_URL = os.environ.get("DB_URL", "./eve.db")
MEDIA_PATH = os.environ.get("MEDIA_PATH", "./media")

app = FastAPI()
app.mount("/media", StaticFiles(directory=MEDIA_PATH), name="media")
app.mount("/static", StaticFiles(directory="app/build/static/"), name="static")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

engine = create_async_engine(f"sqlite+aiosqlite:///{DB_URL}", echo=DEBUG)


def chunk_generator_from_stream(stream, chunk_size, start, size):
    bytes_read = 0

    stream.seek(start)

    while bytes_read < size:
        bytes_to_read = min(chunk_size, size - bytes_read)
        yield stream.read(bytes_to_read)
        bytes_read = bytes_read + bytes_to_read

    stream.close()


def get_file_and_total_size(id):
    stream = open(f"{MEDIA_PATH}/tracks/{id}", mode="rb")
    return stream, os.path.getsize(stream.name)


@app.get("/")
async def root():
    return FileResponse("app/build/index.html")


@app.get("/{path}")
async def root(path: str):
    return FileResponse(f"app/build/{path}")


@app.get("/api/tracks/")
async def get_tracks() -> List[Track]:
    async with engine.begin() as conn:
        tracks = await conn.execute(text("SELECT * FROM tracks"))
        return [dict(t) for t in tracks]


@app.get("/api/poop/{id}")
async def root(id: str, req: Request):
    # Ref: https://github.com/tiangolo/fastapi/issues/1240
    # TODO: Tidy up this logic
    asked = req.headers.get("Range")

    stream, total_size = get_file_and_total_size(id)

    start_byte_requested = int(asked.split("=")[-1][:-1])
    end_byte_planned = min(start_byte_requested + BYTES_PER_RESPONSE, total_size) - 1

    chunk_generator = chunk_generator_from_stream(
        stream,
        chunk_size=10000,
        start=start_byte_requested,
        size=BYTES_PER_RESPONSE
    )
    return StreamingResponse(
        chunk_generator,
        headers={
            "Accept-Ranges": "bytes",
            "Content-Range": f"bytes {start_byte_requested}-{end_byte_planned}/{total_size}",
        },
        status_code=206
    )
