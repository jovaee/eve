import json
import os
import pafy
import sqlite3
import shutil
import requests

from clients.youtube import YouTubeClient


def create_youtube_client() -> YouTubeClient:
    """
    Create YouTube client instance
    """
    with open('secret.json', 'r') as secret:
        api_key = json.load(secret)['api_key']
        return YouTubeClient(api_key)


def main():
    # Create DB connection
    conn = sqlite3.connect("eve.db")

    sql = """
    INSERT INTO tracks (id, title, author, duration) VALUES (:id, :title, :author, :duration)
    ON CONFLICT (id) DO UPDATE SET title = :title, author = :author, duration = :duration
    """

    client = create_youtube_client()
    videos = client.get_playlist_videos("PLdCAB8T82GPoYsO8mb1azGNLu0Ee318mu")
    for video in videos:
        try:
            thing = pafy.new(video["url"])
            audio = thing.getbestaudio()

            print(f"Downloading {video['name']}...")

            # Download the audio
            if not os.path.exists(f"./media/tracks/{thing.videoid}"):
                audio.download(f"./media/tracks/{thing.videoid}", quiet=True)
                print("Audio: \033[0;32mDone\033[0m")

            # Download the thumbnail
            if not os.path.exists(f"./media/thumbnails/{thing.videoid}"):
                r = requests.get(thing.bigthumbhd, stream=True)
                if r.status_code == 200:
                    with open(f"./media/thumbnails/{thing.videoid}", 'wb') as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f)
                    
                    print("Thumbnail: \033[0;32mDone\033[0m")
                else:
                    print("Thumbnail: \033[0;31mFailed\033[0m")

            conn.execute(sql, {"id": thing.videoid, "title": thing.title, "author": thing.author, "duration": thing.length})
            conn.commit()
        except OSError as oe:
            print(f"Failed to get {video['name']} due to {str(oe)}")
            continue
        else:
            print(f"Sucessfully got {video['name']}")


if __name__ == "__main__":
    main()