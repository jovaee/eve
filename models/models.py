from pydantic import BaseModel
from typing import Optional


class Track(BaseModel):
    id: str
    title: str
    author: Optional[str] = None
    url: str
    thumbnail: str
