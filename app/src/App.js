import './App.scss';

import React, { useEffect, useState } from 'react'

import Player from './components/Player'
import Playlist from './components/Playlist'

const CONTROL_ACTIONS = Object.freeze(
  {
    ENDED: 1,
    PREV: 2,
    NEXT: 3,
  }
)


function App() {

  const [tracks, setTracks] = useState([])
  const [activeIndex, setActiveIndex] = useState(0)
  const [controlAction, setControlAction] = useState(null)

  function fetchTracks () {
    fetch(`/api/tracks`)
    .then(response => response.json())
    .then(data => {

      let processedTracks = []
      data.forEach(track => {
        processedTracks.push({
          ...track,
          "thumbnail_art_url": `/media/thumbnails/${track.id}`,
          "url": `/api/poop/${track.id}`
        })
      })

      setTracks(processedTracks)
    })
  }

  // Fetch all available tracks
  useEffect(() => {
    fetchTracks()
  }, [])

  useEffect(() => {
    if (controlAction == CONTROL_ACTIONS.ENDED || controlAction == CONTROL_ACTIONS.NEXT) {
      setActiveIndex(previousActiveIndex => previousActiveIndex + 1 < tracks.length ? previousActiveIndex + 1 : previousActiveIndex)
    } else if (controlAction == CONTROL_ACTIONS.PREV) {
      setActiveIndex(previousActiveIndex => previousActiveIndex - 1 >= 0 ? previousActiveIndex - 1 : previousActiveIndex)
    } else {
      // Have to keep hook sequence consistent so this call required
      setActiveIndex(previousActiveIndex => previousActiveIndex)
    }

    // Will cause this effect to run again but is required to allow the same action to be done again
    // TODO: Another way to do this?
    setControlAction(null)
  }, [controlAction])
  
  return (
    <div className="App">
      <Player
        track={tracks[activeIndex]}
        previousCallback={() => setControlAction(CONTROL_ACTIONS.PREV)}
        nextCallback={() => setControlAction(CONTROL_ACTIONS.NEXT)}
        endedCallback={() => setControlAction(CONTROL_ACTIONS.ENDED)}
      />
      <Playlist 
        tracks={tracks}
        activeIndex={activeIndex}
        onItemClick={index => setActiveIndex(index)}
      />

    </div>
  )
}

export default App;
