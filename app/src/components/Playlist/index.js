import './index.scss';

import { durationToDisplay } from '../../util'


function Playlist({ tracks, activeIndex, onItemClick }) {
  
  function trackItem(track, index) {
      const isPlayingTrack = index === activeIndex
      return (
        <div 
          className={`track ${isPlayingTrack ? "now-playing" : null}`}
          onClick={() => onItemClick(index)}
        >
          <div className="icon-container">
            {!isPlayingTrack ? <img className="play" src="play.svg"/> : null}
            {isPlayingTrack ? <img className="now-playing" src="headphones.svg"/> : null}
          </div>
          <div className="meta-data">
            <span className={`title ${isPlayingTrack ? "bold" : null}`}>{track.title}</span>
            <span className="artist">{track.author}</span>
          </div>

          <div>
            <span className="duration">{durationToDisplay(track.duration)}</span>
          </div>
        </div>
      )
  }

  return (
    <div className="playlist">
      {tracks.map(trackItem)}
    </div>
  )
}

export default Playlist