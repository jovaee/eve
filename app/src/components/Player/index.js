import './index.scss';

import React, { useEffect, useState, useRef, Fragment } from 'react'
import { durationToDisplay } from '../../util'


function Player({ track, playPauseCallback, previousCallback, nextCallback, endedCallback }) {

  const [playing, setPlaying] = useState(false)
  const [currentTime, setCurrentTime] = useState(0)
  const [showVisualizer, setShowVisualizer] = useState(false)
  const audioRef = useRef(null)
  const canvasRef = useRef(null)
  const progressRef = useRef(null)

  // ------- Functions
  function updateCurrentTime(currentTime) {
    // % 1 to remove the milliseconds etc
    setCurrentTime(currentTime - currentTime % 1)
  }

  function seek(e) {
    const offset = progressRef.current.getBoundingClientRect();
    const x = e.pageX - offset.left;
    const percentage = ( parseFloat( x ) / parseFloat( progressRef.current.offsetWidth) ) * 100

    audioRef.current.currentTime = track.duration * percentage / 100
  }

  function setupAnalyzer() {
    // TODO: For now this works but probably needs some improvements
    // * Render bars better
    // * Better color separation between lows and highs
    // * When to actually connect and disconnect stuff to save on cpu
    if (!window.audioContext) {
      window.audioContext = new AudioContext()
      window.src = window.audioContext.createMediaElementSource(audioRef.current)
    }

    let context = window.audioContext
    let src = window.src
   
    let analyser = context.createAnalyser()

    let canvas = canvasRef.current
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    let ctx = canvas.getContext("2d")
    
    src.disconnect()
    src.connect(analyser)
    analyser.connect(context.destination)

    analyser.fftSize = 128

    let bufferLength = analyser.frequencyBinCount

    let dataArray = new Uint8Array(bufferLength)

    let WIDTH = canvas.width
    let HEIGHT = canvas.height

    let barWidth = (WIDTH / bufferLength) * 2.5
    let barHeight
    let x = 0

    function renderFrame() {
      requestAnimationFrame(renderFrame)
  
      x = 0
  
      analyser.getByteFrequencyData(dataArray)
  
      ctx.fillStyle = "#000"
      ctx.fillRect(0, 0, WIDTH, HEIGHT)
  
      for (let i = 0; i < bufferLength; i++) {
        barHeight = dataArray[i] * 2
        
        let r = barHeight + (25 * (i/bufferLength))
        let g = 250 * (i/bufferLength)
        let b = 50
  
        ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")"
        ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight)
  
        x += barWidth + 1
      }
    }

    renderFrame()
  }

  // ------- Hooks
  useEffect(() => {
    audioRef.current.src = track?.url

    if (playing) {
      audioRef.current.play()
    }

    if (showVisualizer) {
      setupAnalyzer()
    }
  }, [track])

  useEffect(() => {
    if (playPauseCallback) {
      playPauseCallback(playing)
    }

    if (playing) {
      audioRef.current.play()
    } else {
      audioRef.current.pause()
    }
  }, [playing])

  useEffect(() => {
    if (showVisualizer) {
      setupAnalyzer()
    } else {
      // window.src?.disconnect()
    }
  }, [showVisualizer])

  useEffect(() => {
    // Make sure nothing is played when it is loaded
    audioRef.current.pause()

    // Add event listeners to usefull stuff
    audioRef.current.addEventListener('timeupdate', () => updateCurrentTime(audioRef.current.currentTime))
    audioRef.current.addEventListener('ended', endedCallback)

    return () => {
      // Makes sure audio stops playing when dismounted
      audioRef.current.src = null

      // Remove all event listeners. TODO: Required to do this?
      audioRef.current.removeEventListener('timeupdate')
      audioRef.current.removeEventListener('ended')
    }
  }, [])

  return (
    <Fragment>
      {/* 
        This audio element might have to be made permanent somehow in the future.
        For exmaple keep playing in the background while managing playlists etc
        No idea how to do this :'(
      */}
      <audio ref={audioRef} />
      <div className="player">
        {showVisualizer ? <canvas className="visualizer" ref={canvasRef}/> : null}
        {!showVisualizer ? <img className="thumbnail" src={track?.thumbnail_art_url}/> : null}
        <div className="bottom-container">
          <progress className="track-progress" ref={progressRef} onClick={seek} value={currentTime} max={track?.duration}></progress>

          <div className="time-container">
            <span className="current-time">
              <span>{durationToDisplay(currentTime)}</span>
            </span>
            <span className="duration">
              <span>{durationToDisplay(track?.duration)}</span>
            </span>
          </div>

          <div className="control-container">
            <div className="control-border prev" onClick={previousCallback} />
            <div className={`control-border play-pause ${playing ? 'playing' : 'paused'}`} onClick={() => setPlaying(!playing)} />
            <div className="control-border next" onClick={nextCallback} />

          </div>

          <div>
            <button className="toggle-visualizer" onClick={() => setShowVisualizer(!showVisualizer)}>Toggle</button>
          </div>

          <div className="meta-container">
            <span className="track-name">{track?.title}</span>
            <span className="track-author">{track?.author}</span>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Player