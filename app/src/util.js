export const durationToDisplay = (duration) => {
    const minutes = Math.floor(duration / 60)
    const minutesDsiplay = `00${minutes}`.slice(-2)
    const secondsDisplay = `00${duration - (60 * minutes)}`.slice(-2)

    return `${minutesDsiplay}:${secondsDisplay}`
}