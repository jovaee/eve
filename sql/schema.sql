CREATE TABLE tracks (
	id TEXT NOT NULL PRIMARY KEY,
    title TEXT NOT NULL,
    author TEXT,
    duration INTEGER NOT NULL
)